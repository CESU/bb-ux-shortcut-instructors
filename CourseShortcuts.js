Event.observe(document,"dom:loaded", function() {

	// The new link elements
	var newElementContainer = jQuery('#shortcut-container');

    // Try getting course id from exsisting elements on page.
	var courseId = jQuery('#enterStudentPreviewForm > [name="course_id"]')[0].value;

    if (!courseId || courseId === "") {
    	newElementContainer.remove();
    } else {
    	// The exsisting navDivider element the new elements should be situated below
        var navDivider = jQuery('.navDivider');

        // Sets links according to current course id
        jQuery('#scSendEmail').attr("href", "/webapps/blackboard/email/caret.jsp?family=cp_send_email&course_id=" + courseId);
        jQuery('#scCourseEvaluation').attr("href", "/webapps/auus-aucem-BBLEARN/course/coursequestions?action=templatelist&course_id=" + courseId);
        jQuery('#scGradecenter').attr("href", "/webapps/gradebook/do/instructor/enterGradeCenter?cvid=fullGC&course_id=" + courseId);

        // Adjusts element position
		navDivider.after(newElementContainer);

        // Make the elements visible
        // NB! Elements are invisible by default (set in css), to avoid visual confusion untill the elements are moved
        newElementContainer.show();
    }
});